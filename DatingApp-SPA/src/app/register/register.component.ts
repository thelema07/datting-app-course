import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "../_services/auth.service";
import { error } from "@angular/compiler/src/util";
import { AlertifyService } from "../_services/alertify.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  @Input() valuesFromHome: any;
  @Output() cancelRegister = new EventEmitter();
  model: any = {};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.getValues();
  }

  register() {
    this.authService.register(this.model).subscribe(
      () => {
        this.alertify.success("Registration successful");
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  cancel() {
    this.cancelRegister.emit(false);
  }

  getValues() {
    this.http.get("http://localhost:5000/api/values").subscribe(
      response => {
        this.valuesFromHome = response;
      },
      error => {
        console.log(error);
      }
    );
  }
}
